#' @title Compute demographic indices over all replicates
#'
#' @description
#' \code{demographic_indices_replicate} computes demographic indices over all replicates
#'
#' @details Compute a large set of demographic statistics :
#'n.pop: the Number of individual in population
#'nb.pop: the number of population
#'occ.rate: the ocupancy rate
#'n.meta: The Number of individual in a metapopulation
#'surv.rate: The survival rate
#'nb.ind.col: number of individuals circulated by colonization to a given generation for the entire metapop
#'nb.ind.out.col:  number of individuals who were sent as part of a colonization to a given generation pop by pop
#'nb.ind.in.col: number of individuals who were received as part of a colonization to a given generation pop by pop
#'nb.event.in.col: number of acquisition events at a given generation pop by pop
#'nb.event.out.col: number of donation events as part of a colonization to a generation pop data by pop
#'nb.event: number of one-generation colonization circulation events at the metapop scale
#'autoproduction.rate:  pop pop computation at a given generation: 1 if the population in question did not use the #'outside to reproduce, 0 otherwise.
#' @param pop.data.i demographic information obtained using CMP2CMPAnalyzer
#' @param col.data.i colnames of the pop.data.i object
#' @param nb_pop number of population to use to compute demographic statistics
#' @param carrying.capacity vector indicating the carying capacity of each population
#' @param stat list of statics to compute
#' @return a list of genetic statistics
#' @export
#' @importFrom data.table data.table


demographic_indices_replicate <- function(pop.data.i, col.data.i, nb_pop, carrying.capacity, stat)
{
  # for debbuging
  # pop.data.i=pop.data[[i]]
  # col.data.i=col.data.i[[i]]
  stat.dem <- list()
  for (j in 1:length(pop.data.i))
  {
    stat.dem[[j]] <- demographic_indices(X=pop.data.i[[j]], Y=col.data.i[[j]], nb_pop, carrying.capacity, stat)
  }
  names(stat.dem) <- names(col.data.i)
  stat.dem
}
