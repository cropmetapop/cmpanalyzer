#' @title each
#'
#' @description
#' \code{each} indicates generations step to which the calculations will be made.
#'
#' @details indicates generations step to which the calculations will be made.
#'
#' @param x  a number indicating generation footstep to be considered
#' note that, it is possible to import sepratly these two files. In this case please set the parameters X and  set.log (see bellow).
#' @param X a dataframe or data table containing genotype table provide by CropMetapop
#' @param final.generation this parameter indicate if the final generation must be considered or not. For example,
#'  if we simulate 7 generation and step parameter set to 2, then this function will only consider the generations 2,4,6.
#'  This parameter can allow adding or not the 7th generation.

#' @return return a vector of generation of interest
#'@export
#'@importFrom data.table is.data.table

each <- function(x, X, final.generation)
{
  ################################ function ..   ############################################################
  if (is.data.table(X))
  {
    generations <- as.numeric(length(X[, grep("Gen ", colnames(X))]))
  }
  if (is.numeric(X))
  {
    generations <- length(X)
  }
  if (generations == 0)
  {
    warning("there is no generation")
  }
  if (generations == 0)
  {
    gn <- 0
  }
  if (generations > 0)
  {
    gn <- c(seq(0, generations - 1, x))
  }
  if (final.generation & (generations)!=gn[length(gn)])
  {
    #if (generations/2 == round(generations/2))
    # gn <- c(seq(0, generations - 1, x), generations - 1) else gn <- c(seq(0, generations - 1 , x))
    gn <- c(seq(0, generations - 1, x), generations - 1)
  }
  unique(gn)
}
