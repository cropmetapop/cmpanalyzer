
## Summary of the tests
```{r, include=TRUE,echo=FALSE}
knitr::kable(matrix( data=c("1","1","0",
                            "2","1","1",
                            "3","...","...",
                            "...","","",
                            "10","50","gradient"),byrow=T,nrow=5,ncol=3, dimnames=list(NULL,c("Check number","Number of population","Type of selection"))),align='c')
```



## Check n°1 : one population without selection

### Load the dataset : data_01
```{r}
devtools::install(pkg = "../CropMetapopAnalyser")
library(CMPAnalyzeR)
#load("../data/data_O1.Rmd")
# select directory

setwd("~/Travail/05-Logiciels/Stats/R/packages/CMPanalyzeR/data")
dir="data_01"
# load data
x=readCMPdata(folder=dir, data.type="mono")
```
### Organize and compute summary statistics
```{r}
# Organize data
out.all = CMP2CMPanalyseR(x, n.sample = NULL, step = 1, parr = FALSE ,
                          final.generation = TRUE)
# Compute summary statics
out <- CMPanalyseR(out.all=out.all, stat="hs", save.plot=F, parr=T, nb_cor=3, pair.sample = NULL,bound=NULL,save.rdata=T)

```


## Check n°2 : one population with selection
## Check n°3 : ...
## ...
## ## Check n°10 : 50 populations with gradiant selection
