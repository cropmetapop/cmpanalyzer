---
title: "CMPAnalyser"
author: "Abdel Kader NAINO JIKA, Mathieu THOMAS, Isabelle GOLDRINGER"
date: '`r Sys.Date()`'
abstract: 'This vignette provides an introductory tutorial to CrompMetapopAnalyseR
  package.It handle handle multilocus and monolocus data and provides an overview
  of functionalities.  '
output:
  pdf_document:
    number_sections: yes
    toc: yes
    toc_depth: 5
  word_document:
    toc: yes
bibliography: kdr.bib
link-citations: yes
subtitle: An introduction to CMPAnalyser
theme: united
includes:
in_header: mystyles.sty
urlcolor: blue
linkcolor: cyan
---



\newpage
\listoffigures

\newpage


```{r pour la fonction kable, include=FALSE}
library(knitr) 
```

# Introduction
This tutorial is a working document that introduces some basic functionalities of CropMetapopAnalyseR package and explain some choices. 
The purpose of this package is to provide tools for handling and analysing CropMetapop output data.

## Warning 1
In this part, I plan to describe the cases that have been tested in order to note some choices I made and some cases that have not been tested yet.


###1- Theoretical scenarios:

- Tomatoes and cabbages (without sampling): I detected bugs in the presence of extinctions. These bugs, linked to empty generations or extinct populations (conséquences in some statistics computations like pairwise Fst and Ne)  have been corrected.


- Wheat and maize (sampling 50 individuals per population because data are massive): I corrected some bugs and retouch some part of the code for efficiency.

###2- Particular cases

- a single population: 
In this case the main problems are due to the computations of some statistics and the transformation of dataframe object into genind doensn't work the same way (bug corrected).

- one single marker


-an entire replicate extinct


-all markers under selection (absence of unselected markers).


- Absence of genetic diversity (for memory, the function Hs of Adegenet used for the calculation of Hs in the  absence of sampling doesen't work if there is no genetic diversity, i put NA in this case instead of 0).


- In the absence of diversity, for the calculation of Ne,some used functions doesn't work , bug correctd by removing the monomorphic markers.


- Because the genetic diversity index (Hs, and by consequence the Fis) is not computed the same ways in the case of sampling or not, and because Ne is correct or not I have also compared the results with sampling and without sampling, that led me to identify and corrected some errors in my code, at the end, I get similar results ( but some times, allelic richness is not the same between multi and monolocus data. This need to be corrected).

###3- Monolocus data Vs Multilocus data:
In the code, i consider monolocus data as a specific case of multilocus data with one single locus, in order to apply the same functions for these two objects, I need to compare every time these two cases. 


For example, some functions don't work (any statistics like MLG (multilocus genotype), MLH (multilocus heterosigotity), cannot be computed for monolocus data). 

Note that some functions in which i do not use any package for computation  like ht, Gst and D, in the case of multilocus data , they are computed for each loci and then mean over loci.

###4- presence and absence of colonization data: 

In the absence of colonization data, I corrected bugs to make code working by putting NULL if there is not colonization data.

###5- Figure 

The functions used to make the figures are tested, if plot are saved, the package produce two figures instead of three:


figure 1- names of the folder+ _demographic_stat_file 


figure 2- names of the folder+ _genetic_stat)


There is a bug when doing figure, especially for effective size. this bug is due to the limit of Y axis. Indeed, indeed some value is infinite. In this case, when I check the maximum value of Ne in order to limit the axis, there an error message. I corrected this bug by adding to conditions the first one is the detection of the maximum value without infinite (but not exactly, see the code), the second is the completion of value (10 x carrying capacity x nb_pop), if it doesn't work then the max will be one million. I did not yet took (10 x metapop) or (10 x nb ind by pop )( to do! )


## Warning 2
We should now look in detail if the calculations are well done (in any case, in case of problem in the calculations it would be enough to correct the problem in the sub-function that does the calculations for a single generation or a replicate (case of ne), this should not impact the rest of the code).
I note a bug in allelic richness computation when comparing multilocus data and monolocus data. I do not know yet if this problem is due to Strarag package i used or not(to do).

It is necessary to use it intensively in order to identify others bugs. Indeed, the bugs are still possible at the level of the figures, especially for the detection of the largest values in order to parameterize the axes of the graph.

The initialization functrion work (for the moment, only works on multi-locus data with the option opt = equal_repeated)

Note that migration data are not analysed yet, but to integrate them, need to copy the same code as colonisation ( it must work)


In the following parts, for each function, there will be two parts, a part that roughly describes the function and a second part that describes in detail the internal functions. This choice to make too many functions was necessary in order to handle multilocus, monolocus, presence or absence of selections, presence of unselected and selected at the same times.


\newpage


# Getting started
## CropMetapop outputs examples

Three genetic data are produced by CropMetapop : 

*multilocus genotypes, monolocus genotypes , haplotypes, colonization data, migration data*. 

## Installing the package

```{r setup, include = TRUE, echo=T}
library(CropMetapopAnalyseR)
# you need to install and load 2 others packages:
library(doMC)
library(data.table)
```

\newpage
## Fonction 1: readCMPdata

CropMetapop Output can be read using readCMPdata function, in addition to the genetic data, we need settinglog. 

```{r ,eval=FALSE, echo=T}
setwd("~/Documents/depot git/output_genetic_indices/outputScript/CropMetapopAnalyser")
x=readCMPdata(folder="data", data.type="mono")
```

the output of the function is a list of four elements: 


**folder.name:**the name of folder


**set.log:**the settinglog


**seed.flow.col:**the colonisation network


**geno.file:**genotypic file. 


```{r, eval=FALSE, echo=T}
x$folder.name
x$set.log
x$seed.flow.col
x$geno.file
```


 
###Arguments


**folder:**Name of the folder containing the CropMetapop output. This file must contain the data setting.log and genetic data. In the absence of one of these two files the function returns an error. Colonization data are optional.


**data.type**1 for  monolocus, 2 for multilocus; il est également possible d’utiliser data.type = "multi" ou data.type = "mono" (I added the possibility of 1 for monolocus and 2 for multilocus to avoid mistakes by writing "mono" or "multi", it happened to me for example not to remember if it is mono or monolocus).
 If there is not  genetic data or requested setting log, there is an error  message saying :
I can not read the genotypic file. Is the given data.type in folder? or



I can not read the setting.log



###Output

a list of 4 elements:


**folder.name**(The name of the folder that contain data)


**set.log**(CropMetapop setting log)


**seed.flow.col**(Colonization data, NULL if no colonization)


**geno.file**(genetic data).


# Fonction 2: CMP2CMPanalyseR


This function transform the CropMetapop output data into CropMetapopAnalyser input.

```{r, eval=FALSE, echo=T}
out.all = CMP2CMPanalyseR(x, n.sample = NULL, step = 1 , parr = TRUE ,final.generation = TRUE, nb_cor = 3)
```


###Arguments


**x :**


Function 1 output


 **n.sample:**
 
 
the number of individuals that would be sampled by population. If Null, there will be no sampling


**step:**


Take as input a unique value corresponding to the generation step on which we would like to work (if = 5, each 5 generations starting with generation 0).
step can also take a vector. for example if step = c (0,3, 5), then this function will only output the data for generations 0, 3 and 5. By default, step = 1 (so works on all generations).


**final.generation:**


If step = unique value, then it would be necessary to indicate whether or not the last generation should be taken into account. For example If we want to work each 5 generation on data including 14 generation, if Final generation = FALSE, then we will work on generation 0,5,10. Whereas if final.generation = T, we will work on: 0,5,10,14.


**parr :**


if = TRUE then parralellization process is used


**nb_cor :**


if  parr = TRUE, it would be necessary to indicate the number of cores which one wishes to use (by default nb_cor = maximum number of cores detected automatically - 1).


Note that In all cases, for demographic data (including colonization), we will work on all generations.


### output
liste de 5 éléments:
```{r, eval=FALSE,echo=T}
out.all$not_selected_markers # by replicate by generation
```


by repicate and by generation, Null if only selected marker



```{r, eval=FALSE, echo=T}
out.all$selected_markers 
```


NULL if only unselected marker


```{r, eval=FALSE, echo=T}
out.all$col_seed_transfert
```

NULL if no colonization


```{r, eval=FALSE, echo=T}
out.all$Ind_numbers_by_pop

```




```{r, eval=FALSE, echo=T}
out.all$param_simu

```

returns a list of 7 elements corresponding to the parameters of the CropMetapop simulations taken from the setting.log and some choices made in
function 2 (Sampling or not, the number of sample if sampling).
These indications will be used in function 3 for statistics computation and to make the figures)

```{r,eval=FALSE, echo=TRUE}
out.all$param_simu$general.information

```


generations, replicates, nb_pop, nb_marker, sel (TRUE sif selection, False if not), data.type (monolocus or multilocus), selfing.rate


```{r,eval=FALSE, echo=TRUE}
out.all$param_simu$selected_markers

```


selected markers name


```{r,eval=FALSE, echo=TRUE}
out.all$param_simu$neutral_markers

```


unselected markers name


```{r,eval=FALSE, echo=TRUE}
out.all$param_simu$generations.opt

```




```{r,eval=FALSE, echo=TRUE}
out.all$param_simu$carr_capacity_optimum

```




Table with population, carriying capacity and optimums


```{r,eval=FALSE, echo=TRUE}
out.all$param_simu$all.data

```

table of two colomn , if  all.data = TRUE, all data are used, if = FALSE, the number of individuals sampled for each population is indicated.

```{r,eval=FALSE, echo=TRUE}
out.all$param_simu$folder.name

```
 
 
 Folder name 



#Fonction 3:  *CMPanalyseR *


This function will analyse data, it calculate genetic and demograpic indexes by replication by metapopulation and by population. It is also possible to represent as a graph form the different results. An argument (save.rda) also allows if desired, to save the  r.data, this is particularly useful in the case where several simulations would like to be compared with each other using *plot.presentation* function.




```{r,eval=FALSE, echo=TRUE}
#out <- CMPanalyseR(out.all, stat ="all.demo"  , pair.sample = 10,  plot = TRUE, save.rdata = TRUE, save.plot = F, bound = NULL, vs.go = F ,global.ne = T,metapop.ne = T, parr = T, nb_cor = NULL)
```


###Arguments


**out.all:**


CropMetapopAnalyser input produce by fonction 2


**stat :**


either vectors comprising the statistics to be calculated from those available (see below), or stat = "all.demo" to calculate all the demographic statistics, stat = "all.genet" for all the genetic stats, or stat = "all" to calculate all available stats.


For Now, the avalable statistics are: 

####Demographic statistics


**n.pop:**


the Number of individual in population


**nb.pop:**


the number of population


**occ.rate:**


the ocupancy rate


**n.meta:**


The Number of individual in a metapopulation


**surv.rate:**


The survival rate


**nb.ind.col:**


number of individuals circulated by colonization to a given generation for the entire metapop


**nb.ind.out.col:**


number of individuals who were sent as part of a colonization to a given generation pop by pop


**nb.ind.in.col:**


number of individuals who were received as part of a colonization to a given generation pop by pop


**nb.event.in.col:**


number of acquisition events at a given generation pop by pop


**nb.event.out.col:**


number of donation events as part of a colonization to a generation pop data by pop


**nb.event:**


number of one-generation colonization circulation events at the metapop scale


autoproduction.rate:  pop pop computation at a given generation: 1 if the population in question did not use the outside to reproduce, 0 otherwise.


#### Genetic statistics


**hs :**


genetic diversity index


**hobs:**


observed heterozygotity


**fis:**


inbreeding coefficient


**ar:**


allelic richness


**si:**


simpson index


**mlg:**


multilocus genotype


**emlg:**


multilocus genotype corrected


**mlh:**


multilocus heterosigocity (note that this index is calculeted by individual and the mean over population, the mean and the variance for each population is indicated in pop stat. pertaining the metapopopulation level, Mlh is the mean over all populations, the variance correspond to variances among populations).


**ht:**total genetic diversity


**fst:**weir and cockeram theta


**p.fst:**pairwise Fst


**gst:**Nei (1983) genetic indexe)


**d:**Jost D


**ne:**effective size
        
        
Note that all these statistics are calculated by metapopulation (by calculating the average and the variance of the indicators calculated by population in addition to the indicators only calculated for the metapopulations) and also for each population.


Note also that all these statistics are computed with the same functions for mono and multilocus data (In the case of monolocus data, these statistics are calculated for each locus and then averaged (sub-function mean.over.loci) on all locus).


**pair.sample:**


**specific for pairwise Fst computation**


The number of populations on which you want to calculate pairwise Fst. (by default pair.sample = 10. In the case where there are several optimums, one will sample for each optimum 10 populations. Note that when you work on monolocus data, we make sure to exactly sample the same populations for each loci.


**plot:**

If TRUE, then the function will output the results as a graph



**save.rdata:**

if TRUE, the results are saved as R.data in the folder that contain Analyzed data


**save.plot:**


If TRUE, then the results are saved in pdf format in the folder, if  False, the plot is displayed in the R console . (ps: I added this option to have the possibility to copy one of the figures displayed in the console, which interests me, to use it in a document without using plot presentation, this option also allows me to work directly on my bug detections without going back and forth on a PDF).


**bound:**


**specific for effective size computation**


This parmeter is used in effective size computation. when bound = NULL, the effective numbers are computed for all generation peers (for example if you use data that have 5 generations (0,1,2,3,4), the effective size will be calculated (for all populations) between generation 0 and generation 1, generation 1 and generation 2, generation 2 and generation 3, generation 3 and generation 5).


bound can be a vector. For example if bound = c (0, 5) then the effective size will be calculated between generation 0 and generation 5 for all populations.
For example if I have 3 generations and 2 populations: the exit will be:


**example**


**vsgo**


**specific for effective size computation**


If TRUE, then the population effective size will be calculated between all population of the first generation and all other populations in all others generation.

**Example**


For example if I have 3 generations and 2 populations: the output will be:


**global.ne**


**specific for effective size computation**


If TRUE calculates the effective size between the first and the last generation for each population and metapopulation


**Metapop.ne**


If TRUE, Then calculate effective size by considering only the generations (all the populations of the generation are mixed and considered as one big population).

### output


liste of 3 éléments + figures (saved or displayed):

```{r}
#out$demographic_stat
```
 

####out$demographic_stat


#####out$demographic_stat$metapop $Replicate$Generation


- 1: nb.pop             


- 2: Nmeta


- 3: survival        


- 4: n.pop.mean        


- 5: n.pop.var 


- 6: occupancy.mean      


- 7: occupancy.var      


- 8: nb.ind.col.mean    


- 9: nb.ind.col.var    


- 10: nb.ind.in.col.mean   


- 11: nb.ind.in.col.var 


- 12: nb.ind.out.col.mean   


- 13: nb.ind.out.col.var    


- 14: nb.event.mean   


- 15: nb.event.var 


- 16: nb.event.in.col.mean  


- 17: nb.event.in.col.var 


- 18: nb.event.out.col.mean 


- 19: nb.event.out.col.var   


- 20: autoproduction.rate.mean  


- 21: autoproduction.rate.var    


####out---demographic_stat---pop---Replicate---Generation


**Example**


Pop n.pop occupancy.rate nb.ind.col  nb.ind.in.col nb.ind.out.col nb.event nb.event.in.col nb.event.out.col autoproduction.rate
  
Note that return NA if a statistic is not calculated.

#### out genetic_stat

#####out$genetic_stat$not_selected_markers

######out$genetic_stat$not_selected_markers$metapop$Replicate$Generation

######out$genetic_stat$not_selected_markers$pop$Replicate$Generation

######out$genetic_stat$not_selected_markers$pairwise_fst

####### out--genetic_stat--not_selected_markers--pairwise_fst--Replicate--Generation1--inter (over population from different optimums)

####### out--genetic_stat--not_selected_markers--pairwise_fst--Replicate--Generation1--intra.XX (over populkations that belong to the same optimum; XX correspond to the optimums rank as indicated in setting log)

#####out---genetic_stat---not_selected_markers---ne---

######out---genetic_stat---not_selected_markers---ne---metapop

######out---genetic_stat--not_selected_markers---ne--metapop--Replicate--ne

######out---genetic_stat--not_selected_markers--ne--metapop--Replicate---global.ne

######out---genetic_stat---not_selected_markers---ne---pop

######out---genetic_stat---not_selected_markers---ne---pop---Replicate---ne

######out---genetic_stat---not_selected_markers---ne---pop---Replicate---global.ne 

**output example for one generation and one replicate**

####out---genetic_stat---selected_markers

#####out---genetic_stat---selected_markers---metapop---Replicate---Generation

#####out---genetic_stat---selected_markers---pop---Replicate---Generation

#####out---genetic_stat---selected_markers---pairwise_fst

######out---genetic_stat---selected_markers---pairwise_fst---Replicate---Generation1---inter (over population from different optimums)

######out---genetic_stat---selected_markers---pairwise_fst---Replicate---Generation1---intra.XX (over populkations that belong to the same optimum; XX correspond to the optimums rank as indicated in setting log)

#####out---genetic_stat---selected_markers---ne

######out---genetic_stat---selected_markers---ne---metapop

######out---genetic_stat---selected_markers---ne---metapop---Replicate---ne

######out---genetic_stat---selected_markers---ne---metapop---Replicate---global.ne

######out---genetic_stat---selected_markers---ne---pop

######out---genetic_stat---selected_markers---ne---pop---Replicate---ne

######out---genetic_stat---selected_markers---ne---pop---Replicate---global.ne 

**output example for one generation and one replicate**

###out---param_simu

Return list of 7 éléments (see function 2)

## How work CropMetapopAnalyser function ?

There is firstly some verifications

1- if there is only one generation and generation.opt is 0 :

return error message: "the generation steps is zero, please verify out.all argument")


2- if pair.sample<=1


return error message: "The number of populations for which the pairwise fst should be calculated can not be less than 2"


3- if there is only one population 


return message : "nb_pop = 1, metapop statistics will not be calculated"
 
 
4- if data.type is monolocus


return message: "you are working on monolocus data, the following statistics will not be calculated: " determin the statistics in stat arg, that cannot be computed for monolocus data ("si", "mlg", "emlg", "mlh").

5- if  bound parameter is not NULL and not a vector


return message: " bound must be a vector or NULL"

6- if number of indicated core is higher than availlable core


return error message: nb_cor is greater than the core number of your computer

## Internal functions


### cbind.na 


cbind inequal column 


###rbind.na


rbind inequal column (usefull for plot)


### harmonic_mean


harmonic mean function, used in the computation of genetic indexe


  harmonic_mean (x, na.rm = TRUE)
  
  
###sub.X2genind


transform genotypic data (multilocus or monolocus) for one generation into genind object


sub.X2genind (X, nb_marker, data.type)


###Functions that calculate stat at one generation


####genetic.indices


compute genetic indices for one generation, the input is one single genind object


  genetic.indices(ind, stat, pop.samples.pfst, all.data)
  
  
####demographic.indices


calculation of demographic stat for one replicate


  demographic.indices (pop.data.i, col.data.i, nb_pop, carryng.capacity, stat)


###Functions that calculate stat for one replicate


####Ne.one.rep


Compute Ne for one replicate


  Ne.one.rep(gen.data.one.rep, bound = NULL, param.simu, vs.go = FALSE, global.ne = T, metapop.ne = F)
  

####genetic.indices.replicate


compute genetic indices for one replicate


  genetic.indices.replicate(genind.input.one.replicate, stat, carr_opt, data.type, pair.sample, all.data)
 
 
###Functions that calculate stat for all replicate


####all.rep.gen.indices


all.rep.gen.indices (gen.data, pop.data, stat, param.simu, pair.sample, parr, nb_cor, bound, vs.go, global.ne)


###Functions used to selected specific metapop, pop, or pairwise statistics (Fst) after their computation


####select_pfst


After calculation of all stat, this unction select in the output the pairwise Fst


select_pfst (out, generations, ...)


####select_stat.metapop


select metapop stats as for pairwise fst


  select_stat.metapop(out, generations, ...)

####select_stat.pop


select pop stats as for pairwise fst
  select_stat.pop(out, generations, ...)
 
  
###Function used to perform plot


Note that , the order of appearance of the plot can be change by changing the order in stat.XX.XX.1 object in the CropMetapopAnalyser function. 


####plot.pop


plot stat comput at population level


plot.pop (out.r.g, param.simu, name.indice, sel, folder)


#### plot.metapop


plot stat comput at metapopulkation level


plot.metapop(out.r.g, param.simu, name.indice, sel, folder)

####plot.pfst

plot pairwise fst


plot.pfst(out.r.g, param.simu, sel, folder)


####plot.Ne


plot Ne


plot.Ne(out.r.g, param.simu, sel, folder, vs.go)


# *plot.presentation* function
This function is important for making figures to use in documents. It allows to display several simulations on a same graphic.
To show the functionality, let's take the example of four simulation scenarios representing four species.

## internernal functions





\newpage
# Appendix
## Computed statistics and underlyng methods

### Demographic statistics

####Demographic statistics


#####The number of individual in a given poopulation: **n.pop:**


n.pop is the Number of individual in each population, it is calculated by population.

(@)  $$n.pop=\sum Ind$$


where ind is the individuals in each population

n.pop.mean is the mean number of individual in non extinct population.

$$n.pop.mean =\frac {\sum n.pop} {nb.pop.a} $$

where $nb.pop.alive is the number of non extinct population.

n.pop.var is the variance of number of individual among non extinct population.

$$n.pop.var =  $$
#####**nb.pop:**


nb.pop is the number of non extinct population


#####**occ.rate:**

occ.rate is the occupancy rate. It is calculated by comparing the number of individuals per population with the carryng capacity for each population.
The package calculates by pop and also gives the average on non-empty population and variance.

Occupancy rate of population $i$ at time $t$:

(@)  $$O_{i}(t)=\frac{n_{i}(t)}{k_{i}}$$
where $n_{i}(t)$ is the number of individual of population $i$ at a time $t$ and $k_{i}$ is the carrying capacity of population $i$.

Average occupancy rate of the populations at time $t$:

(@)  $$O(t)=\frac{1}{N_{pop}(t)}\sum_{i=1}^{N_{pop}(t)}{O_{i}(t)}$$



#####**n.meta:**


The Number of individual in a metapopulation
$$n.meta=\sum_{i=1}^{nb.pop}{n.pop}$$


#####**surv.rate:**

Survival rate ($S(t)$) is calculated by making the ratio of the number of existing population at a time t and the total number of populations (50 in our case). $S(t)$ ranges from 0 (when all farms are empty) to 1 (when all farms are full). 

(@)  $$S(t)=\frac{N_{pop}(t)}{N_{farm}}$$

where $N_{pop}(t)$ is the number of living populations at a time $t$ and $N_{farm}$ is the total number of farms.


#####**nb.ind.out.col:**


$nb.ind.out.col$ is the number of individuals who were sent as part of a colonization to a given generation pop by pop. 

(@)  $$nb.ind.out.col= \sum ind.out  $$


where$ind.out$ is the number of individual sent to another population


(@)  $$nb.ind.out.col.mean = \frac {\sum nb.ind.out.col} {nb.pop.a}$$



where $nb.pop.a$ is the number of population that effectivly sent individual to another populations.

#####**nb.ind.in.col:**

where$ind.in$ is the number of individual receive by a given population from another one


(@)  $$nb.ind.in.col= \sum_{i}^{nb.pop.a} ind.in  $$


(@)  $$nb.ind.in.col.mean = \frac {\sum nb.ind.in.col} {nb.pop.a}$$ 


where $nb.pop.a$ is the number of population that effectivly sent individual to another populations.



#####**nb.ind.col:**

$nb.ind.col$ is the total number of individuals circulated by colonization process to a given generation for the entire metapop

(@)   $$nb.ind.col = nb.ind.in.col + nb.ind.out.col $$


(@)  $$nb.ind.mean = \frac {\sum nb.ind.in.col} {nb.pop.a}$$


where $nb.pop.a$ is the number of population that effectivly sent individual to another populations.



#####**nb.event.in.col:**


$nb.event.in.col$ is the number of acquisition events at a given generation pop by pop


#####**nb.event.out.col:**


$nb.event.out.col$ is the number of donation events as part of a colonization to a generation pop data by pop


#####**nb.event:**


$nb.event$ number of one-generation colonization circulation events at the metapop scale


#####**autoproduction.rate:**


$autoproduction.rate$ mesure the level of autoproduction. it equal to 1 if the population did not use the outside to reproduce, 0 otherwise. 


####Genetic statistics
#####Intrapopulation genetic diversity 
Each intrapopulation genetic diversity indicator  is calculated using three methods.

A first method corresponding to an exact calculation, carried out when the computation is carried out on exact data, a second method is used when the computation is carried out on a sample. Finally the average of the different indicators is calculated for the metapopulation if computation is made on more than one population.

######Gene diversity : hs

This indicator ranges from 0 (no diversity at all in the population) to 1 (all plants in the population are genetically different).
It corresponds to intrapopulation heterozygosity ($H_s$) computed for each population and averaged over the metapopulation.
In each population, it is calculated by locus and averaged over all the loci.
Depending on the case, two methods of calculation are used.

When the calculation is carried out on the totality of the data, the following formula (@Nei1973) is used:

(@) $$H_s=1 - \sum_{k=1}^K p^2$$

where $K$ is the number of loci and $P$ is the allelic frequency.



When individuals in the populations come from a sample, method proposed by Nei @Nei1983 (equation 9) is used.


(@) $$H_{s (hierf)}= ({\tilde{n}}/{{\tilde{n}}-1}) [1-\sum_{i} \bar{p_i^2}-H_0/2\tilde{n}]$$



(@)  $\tilde{n}= np / \sum_{k}  1/nk$ et $\bar{p_i^2}=\sum_{k} {p_i^2}/np$

######Observed Heterozygosity: hobs

$h_{obs}$ is the observed heterozygosity. It is the ratio of the number of heterozygotes at a locus to the total number of individuals sampled. It is calculated for each locus then averaged over all the loci.

(@) $$H_{obs}= \frac {1}{k}  \sum_{k=1}^K \Bigg(\frac {n.het}{N}\Bigg)$$

where K is the number of loci, n.het the number of heterozygote at a given loci, N the population size.


######Fixation index: Fis

The inbreeding coefficient $Fis$ is calculated as follows:

(@)  $$ F_{is} = 1- \frac { \bar H_{obs}}{H_s}$$


When Hs correspond to gene diversity  computed using @Nei1973 or @Nei1983, depending on the data type (Sample or all data), and $H_{obs}$ the observed heterosigosity 


######The allalic richness: ar

In this package the computed allelic richness is not corrected.

(@)  $$ar = \frac {N.alleles}{nb.pop}$$

where $N.alleles$ is the number of alleles in the population and $nb.pop$ is the number of population.


######Simpson index:si

The Simpson index is computed only for multilocus genotype data type.
It corresponds to Simpson's unbiased index.
It is calculated from $\lambda$ (biaised simpson index, SIMPSON 1949).
The simpson index is here an estimate of the probability that two genotypes taken at random in a population are different and then correspond to the $Hs$ of @Nei1978  ($Hs$ corrected by $N/N-N$, where $N$ is the population size)

(@)  $$Si= 1-\sum_{k=1}^n p^2  \Bigg(\frac{N}{N-1}\Bigg)  $$

When $K$ is the number of population and $N$ the population size.

$s_i$ varies between 0 (no genotype is different) and 1 (all genotypes are different).




######multilocus genotype: **mlg** and **emlg**

The multilocus genotype $mlg$ is the number of observed multilocus genotypes, that is, the number of unique combinations of alleles in all loci.


The emlg is a Mlg corrected by the size of the population following the rarefaction method.
therefore, the number of MLGs expected for the smallest sample size (Hurlbert et al., 1971)^[see also: \href{https://cran.r-project.org/web/packages/poppr/vignettes/poppr_manual.html}{\textcolor{blue}{lien }} ] 



######Multilocus heterozygotity: **mlh**


The multilocus heterosigocity $mlh$ is the ratio of the total number of heterozygous loci of an individual to the total number of loci.
it is then calculated by individual using the following formula:

(@) $$mlh= \frac{1}{n}  \sum_{n=1}^{n}  \Bigg(\frac{n.het.i}{k}\Bigg)$$

where n is the number of individual in a given population,n.het.i is the total number of heterozygous loci of an individual and k the number of typed loci for the individual.
An MLH of 0.5  means that 50% of the loci in the individual are heterozygous.

In this package, note that this index is calculeted by individual and then mean over population, the mean and the variance for each population is indicated in pop stat. pertaining the metapopopulation level, Mlh is the mean over all populations, the variance correspond to variances among populations).


#####Interpopulation genetic diversity 


######The total genetic diversity: \textcolor{red}{ht}


It corresponds to  heterozygosity ($H_t$) computed at the metapopulation level, without taking into acount the population structure for each population and then averaged over the metapopulation.

In each population, it is calculated by locus and averaged over all the loci using the same formula as in $H_s$:

(@) $$H_s= \frac{1}{L} \sum_{l=1}^L (1 - \sum_{k=1}^K p_{k,l}^2)$$

where K is the number of loci and P is the allelic frequency.

as $H_s$, it ranges from 0 (no diversity at all in the métapopulation) to 1 (all plants are genetically different).

######Global level of divergence among populations

The global level of divergence among populations of the same metapopulation is obtained with various indicators.

#######The weir and cockeram theta :\textcolor{red}{fst}

We estimate $F_{ST}$ by calculating $\theta$, an estimator developed by Weir and Cockerham @Weir1984.

The genetic differentiation is computed at the metapopulation level following the variance decomposition used by Weir and Cockerham [-@Weir1984]:

(@) $$F_{ST}(t)=\frac{\sigma^2_a(t)}{\sigma^2_a(t)+\sigma^2_b(t)+\sigma^2_w(t)}$$

where $\sigma^2_a(t)$ is the variance among populations, $\sigma^2_b(t)$ is the variance among individuals within population,  $\sigma^2_w(t)$ is the variance among genes within individual.

The Pairwise Fst is the Fst value computed by pair of population.

#######**gst:**Nei (1983) genetic indexe)

The $g_{st}$ is calculated as proposed by @Nei1983

(@) $$g_{st} =\frac {(H_t-H_s)}{(H_t)} ]$$

where $H_s$ and $H_t$ are the unbiased estimators of $H_s$ and $H_t$  calculated from the formulas given in this document

#######**d:**Jost D

It corresponds to the jost (D), it is calculated with the following formula:

$$d= \frac{H_t - H_s}{1 - H_s} * \frac{Nb.pop}{Nb.pop - 1}$$
where $H_s$ and $H_t$ are the unbiased estimators of $H_s$ and $H_t$  calculated from the formulas given in this document, Nb.pop is the number of populations.


#######The Population effetive size **ne:**

The effective size is calculated using wapples formula as folow:

If calculated on all data


(@) $$ne = \frac{\delta (t)}{2 \hat Fc} $$





\newpage
\noindent {\color{RubineRed} \rule{\linewidth}{1mm} }
