##########################################
######## Main initialization
################################################
rm(list = ls())

Input_folders <- c("vignette_main")

#Input_folders <- c("tomato_france_ring_no-selection_2018_3_3_19:59:42")
#tom_highdiv_lowstress_indep
#italie_sel
source("func/CropMetapopAnalyser_V001/all.functions.r")


#stat=c("n.pop","occ.rate","n.meta","surv.rate","hs","hobs","fis","ar","si","mlg","emlg","mlh","ht","fst","p.fst","gst","d")
stat="all"
#stat <- c("n.pop", "occ.rate", "n.meta", "surv.rate", "hs", "hobs", "fst", 
#          "ht", "gst", "d","nb.pop")
#stat <- c("occ.rate", "surv.rate", "fst", "hs")
#stat <- c("surv.rate", "p.fst")
t <- 1


for (t in 1:length(Input_folders))
{
  folder <- Input_folders[t]
  path.setfilog <- paste(folder, "/setting.log", sep = "")
  set.log <- read.table(path.setfilog, sep = "/")
  infile <- paste(Input_folders[t], "/GenotypeMono.csv", sep = "")
  X <- fread(infile, sep = ",", h = TRUE, fill = TRUE)
  tic()
  out.all <- df2list(X, n.sample = 30, set.log = set.log, step = 1, parr = T)
  toc()
  
  tic()
  out <- extract_sumstat_fast(out.all, stat, pair.sample = 10, folder, 
                              parr = T, plot = TRUE, save.rdata = TRUE, save.plot = T)
  toc()
}
