# général
- tester cas limites : extinction de pop, nb de pop = 1, avec mrk sous selection
- corriger bug difference entre mono et multi : valeurs proches  jusqu'à 3 chiffres après la virgule
- essayer de virer l'usage de StrataG : reste à gérer pairwiseFst
- initialiser ls.stat.demo et ls.stat.genet une fois pour toute (lors du chargement de CMPanalyseR ?)
- comment est géré la mémoire allouée à une variable locale à une fonction quand on sort de la fonction
- ajouter drop=FALSE en remplacement des condition npop == 1
***** - gérer les cas sel / nsel / both NB penser a metre a j sum_param_simu et CMP2R ****** EN COURS
- parallelisation Unix/Windows unifiée : ok
  a - remplacement du paquet R doMC par doParallel
  b - sous Unix :    //isation par 'fork clustering'   => parallel::makeCluster(..., type="FORK")
      sous Windows : //isation par 'socket clustering' => parallel::makeCluster(..., type="PSOCK")
      (voir https://www.blasbenito.com/post/02_parallelizing_loops_with_r/)
  c - remplacement de mclapply() par parLapply()

# fonction CMP2R
- remplacer mono/multi monolocus/multilocus : ok
- regrouper les paramètres parr + nbcor
- supprimmer la colonne inutile Replicate dans l'objet gen.data$ReplicateN$Gen.t$Replicate

# fonction CMPanalyzer
- debugger génération pdf stat genet : ok

# fonction df2genind_rep_gen
- factoriser boucle for : ok

# fonction all_rep_gen_indices
- factoriser le bloc de code if(parr == FALSE ou TRUE)

# fonction init_sampling
- revoir sample vs dplyr::sample_n

# sampling_ind
- rajouter option seed pour reproductibilité out.all : ok

# test_profiling
- Ajout de procédure de test en mono et multi locus : ok

# genetic_indices
- remplacer les variances par sd : ok
